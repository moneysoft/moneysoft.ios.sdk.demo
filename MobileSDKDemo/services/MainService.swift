//
//  MainMenuService.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class MainService: ServiceProtocol {
	func back() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.AUTHORISATION);
		}
		catch {
			print(error);
		}
	}
	
	func getEndpoints() -> [ApiEndpoint] {
		return [
			ApiEndpoint(name: MainMenuEndpoint.FINANCIAL.rawValue,
						action: {
							self.loadFinancialEndpoints();
						}),
			ApiEndpoint(name: MainMenuEndpoint.TRANSACTIONS.rawValue,
						action: {
							self.loadTransactionEndpoints();
						}),
//			ApiEndpoint(name: MainMenuEndpoint.USER.rawValue,
//						action: {
//							self.loadUserEndpoints();
//						})
		];
	}
	
	private func loadFinancialEndpoints() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.FINANCIAL);
		}
		catch {
			print(error);
		}
	}
	
	private func loadTransactionEndpoints() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.TRANSACTIONS);
		}
		catch {
			print(error);
		}
	}
	
	private func loadUserEndpoints() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.USER);
		}
		catch {
			print(error);
		}
	}
}
