//
//  TransactionService.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import MobileSDK

class TransactionService: ServiceProtocol {
	func back() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.MAIN);
		}
		catch {
			print(error);
		}
	}
	
	func getEndpoints() -> [ApiEndpoint] {
		return [
			ApiEndpoint(name: TransactionEndpoint.GET_TRANSACTIONS.rawValue,
						action: {
							self.getTransactions();
						}),
			ApiEndpoint(name: TransactionEndpoint.CATEGORIES.rawValue,
						action: {
							self.getCategories();
						})
		];
	}
	
	private func getTransactions() {
		let FINANCIAL_ACCOUNT_KEY: Int = 0;
		let START_DATE_KEY: Int = 1;
		let END_DATE_KEY: Int = 2;
		let COUNT_KEY: Int = 3;
		let OFFSET_KEY: Int = 4;
		let inputDialog: InputDialogController = MANAGER.Dialogs.createInputDialog();

		inputDialog.header = "Filter";
		
		let accountFld: UITextField = UITextField();
		accountFld.borderStyle = UITextBorderStyle.line;
		accountFld.autocapitalizationType = UITextAutocapitalizationType.none;
		
		inputDialog.addInput(label: "Financial Account:",
							 component: accountFld,
							 tag: FINANCIAL_ACCOUNT_KEY);
		
		let startDateFld: UITextField = UITextField();
		startDateFld.borderStyle = UITextBorderStyle.line;
		startDateFld.autocapitalizationType = UITextAutocapitalizationType.none;
		startDateFld.placeholder = "yyyy-MM-dd";
		
		inputDialog.addInput(label: "Start Date:",
							 component: startDateFld,
							 tag: START_DATE_KEY);
		
		let endDateFld: UITextField = UITextField();
		endDateFld.borderStyle = UITextBorderStyle.line;
		endDateFld.autocapitalizationType = UITextAutocapitalizationType.none;
		endDateFld.placeholder = "yyyy-MM-dd";
		
		inputDialog.addInput(label: "End Date:",
							 component: endDateFld,
							 tag: END_DATE_KEY);
		
		let countFld: UITextField = UITextField();
		countFld.borderStyle = UITextBorderStyle.line;
		countFld.autocapitalizationType = UITextAutocapitalizationType.none;
		
		inputDialog.addInput(label: "Count:",
							 component: countFld,
							 tag: COUNT_KEY);
		
		let pageFld: UITextField = UITextField();
		pageFld.borderStyle = UITextBorderStyle.line;
		pageFld.autocapitalizationType = UITextAutocapitalizationType.none;
		
		inputDialog.addInput(label: "Page:",
							 component: pageFld,
							 tag: OFFSET_KEY);
		inputDialog.onSuccess {
			let dateFormatter: DateFormatter = DateFormatter();
			let filters: TransactionFilter = TransactionFilter();
			
			dateFormatter.dateFormat = "yyyy-MM-dd";
			
			for field in inputDialog.inputs {
				let value: String? = (field as? UITextField)?.text;
				
				if (value != nil) {
					switch (field.tag) {
					case FINANCIAL_ACCOUNT_KEY:
						filters.financialAccountId = Int(value!) ?? -1;
						break;
					case START_DATE_KEY:
						filters.fromDate = dateFormatter.date(from: value!);
						break;
					case END_DATE_KEY:
						filters.toDate = dateFormatter.date(from: value!);
						break;
					case COUNT_KEY:
						filters.count = Int(value!) ?? 50;
						break;
					case OFFSET_KEY:
						filters.offset = Int(value!) ?? 0;
						break;
					default:
						break;
					}
				}
			}
			
			self.getTransactions(filter: filters);
		}
		
		inputDialog.show();
	}
	
	private func getTransactions(filter: TransactionFilter) {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: TransactionEndpoint.GET_TRANSACTIONS.rawValue,
																						 message: "Retrieving transactions, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let transactionListener: ApiListListener<FinancialTransactionModel> = ApiListListener<FinancialTransactionModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let transactions: [FinancialTransactionModel] = response as? [FinancialTransactionModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: TransactionEndpoint.GET_TRANSACTIONS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				var transactionsMsg: String = "";

				for transaction in transactions {
					transactionsMsg.append("\(transaction.name ?? "Unknown Name") | \(transaction.amount) | \(transaction.type.rawValue)\n");
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: TransactionEndpoint.GET_TRANSACTIONS.rawValue,
																						   message: transactionsMsg);
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(TransactionEndpoint.GET_TRANSACTIONS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.transactions().getTransactions(filter: filter,
													 listener: transactionListener);
		}
		catch {
			print(error);
		}
	}
	
	private func getCategories() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: TransactionEndpoint.CATEGORIES.rawValue,
																						 message: "Retrieving categories, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let categoryListener: ApiListListener<TransactionCategoryGroup> = ApiListListener<TransactionCategoryGroup>(successHandler: {
			(categoriesResponse) in
				loadingDialog.hide();
			
				guard let groups: [TransactionCategoryGroup] = categoriesResponse as? [TransactionCategoryGroup] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: TransactionEndpoint.CATEGORIES.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				var categoryList: String = "";

				for group in groups {
					categoryList.append("\(group.name ?? "Unknown Group")\n");

					for category in group.categoryList {
						categoryList.append("\t\(category.name ?? "Unknown Category")\n");
					}
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: TransactionEndpoint.CATEGORIES.rawValue,
																						   message: categoryList);
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(TransactionEndpoint.CATEGORIES.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.transactions().getCategoryGroups(listener: categoryListener);
		}
		catch {
			print(error);
		}
	}
}
