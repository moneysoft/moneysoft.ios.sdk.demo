//
//  ServiceProtocol.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 20/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation

protocol ServiceProtocol {
	func back();
	func getEndpoints() -> [ApiEndpoint];
}
