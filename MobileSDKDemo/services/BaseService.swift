//
//  BaseService.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit
import MobileSDK

class BaseService {
	internal var rootController: BaseController? = nil;
	internal var loadingController: LoadingDialogController? = nil;
	
	init(rootController: BaseController?) {
		self.rootController = rootController;
	}
	
	
}
