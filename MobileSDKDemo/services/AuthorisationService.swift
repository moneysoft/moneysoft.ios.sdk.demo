//
//  AuthorisationService.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit
import MobileSDK

class AuthorisationService: ServiceProtocol {
	func back() {
		// Nothing to do as we can't go back
	}
	
	func getEndpoints() -> [ApiEndpoint] {
		return [
			ApiEndpoint(name: AuthorisationEndpoint.ENVIRONMENT.rawValue,
						action: {
							self.chooseEnvironment();
						}),
			ApiEndpoint(name: AuthorisationEndpoint.LOGIN.rawValue,
						action: {
							self.login();
						}),
			ApiEndpoint(name: AuthorisationEndpoint.LOGOUT.rawValue,
						action: {
							self.logout();
						})
		];
	}
	
	func chooseEnvironment() {
		let optionsDialog: OptionsDialogController<[String : String]> = MANAGER.Dialogs.createOptionsDialog();
		
		optionsDialog.header = "Select Environment";
		optionsDialog.addOption(key: "Moneysoft Beta", value: [ "https://api.beta.moneysoft.com.au" : "https://pfm.beta.moneysoft.com.au" ]);
		optionsDialog.addOption(key: "Cheq", value: [ "https://api.cheq.moneysoft.com.au" : "https://cheq.moneysoft.com.au" ]);
		
		optionsDialog.onSelect {
			(selections) in
			guard let selectedEnvironments: [[String : String]] = selections else {
				print("Unable to determine the selected environments.");
				return;
			}
			
			if (selectedEnvironments.count > 0) {
				let selectedEnvironment: [String : String]? = selectedEnvironments.first;
				
				if (selectedEnvironment != nil) {
					let config = MoneysoftApiConfiguration.init(apiUrl: selectedEnvironment?.first?.key ?? "",
																apiReferrer: selectedEnvironment?.first?.value ?? "",
																view: UIView(),
																isDebug: true,
																isBeta: true);
					
					MoneysoftApi.configure(config);
				}
				else {
					print("No environment selected.");
				}
			}
			else {
				print("No environments selected.");
			}
		}
		
		optionsDialog.show();
	}
	
	private func login() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: AuthorisationEndpoint.LOGIN.rawValue,
																						 message: "Attempting login...");
		loadingDialog.alertTitle = AuthorisationEndpoint.LOGIN.rawValue;
		
		let msApi: MoneysoftApi = MoneysoftApi();
		let authenticationListener: ApiListener<AuthenticationModel> = ApiListener<AuthenticationModel>(successHandler: {
			(authenticationResponse) in
				guard let _ = authenticationResponse else {
					return;
				}
			
				MANAGER.User = UserManager();
			
				do {
					MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.MAIN);
				}
				catch {
					print(error);
				}
			
				loadingDialog.hide();
		}, errorHandler: {
			(error) in
				MANAGER.User = nil;
				print("login(ERROR): \(String(describing: error?.messages))");
			
				loadingDialog.hide();
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: AuthorisationEndpoint.LOGIN.rawValue,
																						   message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "Unable to log in");
			
				alertDialog.show();
		});

		let inputDialog: InputDialogController = MANAGER.Dialogs.createInputDialog();
		
		inputDialog.header = "Enter Credentials";
		
		let usernameFld: UITextField = UITextField();
		usernameFld.borderStyle = UITextBorderStyle.line;
		usernameFld.autocapitalizationType = UITextAutocapitalizationType.none;
		
		inputDialog.addInput(label: "Username",
							 component: usernameFld,
							 tag: 0);
		
		let passwordFld: UITextField = UITextField();
		passwordFld.borderStyle = UITextBorderStyle.line;
		passwordFld.isSecureTextEntry = true;
		passwordFld.autocapitalizationType = UITextAutocapitalizationType.none;
		
		inputDialog.addInput(label: "Password",
							 component: passwordFld,
							 tag: 1);
		
		inputDialog.onSuccess {
			do {
				loadingDialog.show();
				try msApi.user().login(username: usernameFld.text ?? "",
									   password: passwordFld.text ?? "",
									   listener: authenticationListener);
			}
			catch {
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: AuthorisationEndpoint.LOGIN.rawValue,
																						   message: "Unable to log in");
				
				alertDialog.show();
			}
		}
		
		inputDialog.show();
	}
	
	private func logout() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: AuthorisationEndpoint.LOGOUT.rawValue,
																						 message: "Attempting logout...");
		loadingDialog.alertTitle = AuthorisationEndpoint.LOGOUT.rawValue;
		
		let msApi: MoneysoftApi = MoneysoftApi();
		
		do {
			loadingDialog.show();
			try msApi.user().signOut();
			loadingDialog.hide();
		}
		catch {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: AuthorisationEndpoint.LOGOUT.rawValue,
																					   message: "Unable to sign out");
			
			alertDialog.show();
		}
	}
}
