//
//  FinancialService.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 20/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import MobileSDK

class FinancialService: ServiceProtocol {
	private var selectedInstitution: FinancialInstitutionModel? = nil;
	private var credentialForm: InstitutionCredentialsFormModel? = nil;
	private var selectedAccounts: [FinancialAccountLinkModel]? = nil;
	
	func back() {
		do {
			MANAGER.Service.activeService = try ServiceFactory().create(ServiceType.MAIN)
		}
		catch {
			print(error)
		}
	}
	
	func getEndpoints() -> [ApiEndpoint] {
		return [
			ApiEndpoint(name: FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue,
						action: {
							self.getInstitutions();
						}),
			ApiEndpoint(name: FinancialEndpoint.INSTITUTION_PROMPTS.rawValue,
						action: {
							self.getPrompts();
						}),
			ApiEndpoint(name: FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue,
						action: {
							self.prepareGetLinkableAccounts();
						}),
			ApiEndpoint(name: FinancialEndpoint.LINK_ACCOUNTS.rawValue,
						action: {
							self.linkAccounts(accounts: self.selectedAccounts);
						}),
// TODO
//			ApiEndpoint(name: FinancialEndpoint.UPDATE_CREDENTIALS.rawValue,
//						action: {
//							self.prepareUpdateCredentials();
//						}),
			ApiEndpoint(name: FinancialEndpoint.GET_ACCOUNT.rawValue,
						action: {
							self.getAccount();
						}),
			ApiEndpoint(name: FinancialEndpoint.GET_ACCOUNTS.rawValue,
						action: {
							self.getAccounts();
						}),
			ApiEndpoint(name: FinancialEndpoint.REFRESH_ACCOUNT.rawValue,
						action: {
							self.prepareRefreshAccount();
						}),
			ApiEndpoint(name: FinancialEndpoint.REFRESH_ACCOUNTS.rawValue,
						action: {
							self.refreshAccounts();
						}),
			ApiEndpoint(name: FinancialEndpoint.UPDATE_TRANSACTIONS.rawValue,
						action: {
							self.updateTransactions();
						}),
			ApiEndpoint(name: FinancialEndpoint.UNLINK_ACCOUNT.rawValue,
						action: {
							self.unlinkAccount();
						})
		];
	}

	private func getInstitutions() {
		var options: [String : FinancialInstitutionModel] = [ : ];
		let confirmListener: (([FinancialInstitutionModel]?) -> Void) = {
			(selections) in
				guard let institutions: [FinancialInstitutionModel] = selections else {
					return;
				}
			
				let institution: FinancialInstitutionModel? = institutions.first ?? nil;
			
				if (institution != nil) {
					self.selectedInstitution = institution;
				}
		}
		
		let loadingController: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue,
																							 message: "Retrieving institutions, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let institutionsListener: ApiListListener<FinancialInstitutionModel> = ApiListListener<FinancialInstitutionModel>(successHandler: {
			(institutionsResponse) in
				loadingController.hide();
			
				guard let institutions = institutionsResponse as? [FinancialInstitutionModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				for institution in institutions {
					options[institution.name ?? ""] = institution;
				}

				let optionsDialog: OptionsDialogController<FinancialInstitutionModel> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Institutions (\(institutions.count))";
				optionsDialog.options = options;
				optionsDialog.onSelect(handler: confirmListener);
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingController.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue)\n(ERROR)",
																						   message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingController.show();
			try msApi.financial().getInstitutions(listener: institutionsListener);
		}
		catch {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue)\n(ERROR)",
																					   message: "\(error)");
			alertDialog.show();
		}
	}
	
	private func getPrompts() {
		if (selectedInstitution != nil) {
			let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.INSTITUTION_PROMPTS.rawValue,
																							 message: "Retrieving prompts, please wait...");
			let msApi: MoneysoftApi = MoneysoftApi();
			let credentialsFormListener: ApiListener<InstitutionCredentialsFormModel> = ApiListener<InstitutionCredentialsFormModel>(successHandler: {
				(response) in
					loadingDialog.hide();
				
					guard let credentialsForm: InstitutionCredentialsFormModel = response else {
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.INSTITUTION_PROMPTS.rawValue,
																								   message: "Unable to parse the response.");
						alertDialog.show();
						return;
					}
				
					self.credentialForm = credentialsForm;
			}, errorHandler: {
				(error) in
					loadingDialog.hide();
				
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.FINANCIAL_INSTITUTIONS.rawValue)\n(ERROR)",
						message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
					alertDialog.show();
			});
			
			do {
				loadingDialog.show();
				try msApi.financial().getSignInForm(institution: selectedInstitution!,
													listener: credentialsFormListener);
			}
			catch {
				print(error);
			}
		}
		else {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.INSTITUTION_PROMPTS.rawValue)\n(ERROR)",
				message: "Please select an institution first.");
			
			alertDialog.show();
		}
	}
	
	private func prepareGetLinkableAccounts() {
		if (credentialForm != nil) {
			let inputDialog: InputDialogController = MANAGER.Dialogs.createInputDialog();
			
			inputDialog.header = "Sign In";
			
			for prompt in credentialForm?.prompts ?? [] {
				let input: UITextField = UITextField();
				
				input.borderStyle = UITextBorderStyle.line;
				input.autocapitalizationType = UITextAutocapitalizationType.none;
				
				if (prompt.type == InstitutionCredentialPromptType.PASSWORD) {
					input.isSecureTextEntry = true;
				}
				
				inputDialog.addInput(label: prompt.label,
									 component: input,
									 tag: prompt.index);
			}
			
			inputDialog.onSuccess {
				let taggedInputs: [UIView] = inputDialog.inputs.filter({
					(view) -> Bool in
						return (view.tag > -1);
				});
				
				for input in taggedInputs {
					let matchingPrompt: InstitutionCredentialPromptModel? = self.credentialForm?.prompts.first(where: {
						(prompt) -> Bool in
							return (prompt.index == input.tag);
					});
					
					matchingPrompt?.savedValue = (input as! UITextField).text;
				}
				
				self.getLinkableAccounts();
			}
			
			inputDialog.show();
		}
		else {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue)\n(ERROR)",
				message: "Please call \"Get Prompts\" first.");
			
			alertDialog.show();
		}
	}
	
	private func getLinkableAccounts() {
		if (credentialForm != nil) {
			let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue,
																							 message: "Retrieving linkable accounts, please wait...");
			let msApi: MoneysoftApi = MoneysoftApi();
			let linkAccountListener: ApiListListener<FinancialAccountLinkModel> = ApiListListener<FinancialAccountLinkModel>(successHandler: {
				(linkResponse) in
					loadingDialog.hide();
				
					guard let accounts: [FinancialAccountLinkModel] = linkResponse as? [FinancialAccountLinkModel] else {
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue,
																								   message: "Unable to parse the response.");
						alertDialog.show();
						return;
					}
				
					var options: [String : String] = [ : ];
				
					for account in accounts {
						if (account.name != nil && account.providerAccountId != nil) {
							options[account.name!] = account.providerAccountId!;
						}
					}
				
					let confirmListener: (([String]?) -> Void) = {
						(selections) in
							guard let accountIds: [String] = selections else {
								return;
							}
						
							self.selectedAccounts = [];
						
							for account in accounts {
								if (account.providerAccountId != nil
									&& accountIds.contains(account.providerAccountId!)) {
									self.selectedAccounts?.append(account);
								}
							}
					};
				
					let optionsDialog: OptionsDialogController<String> = MANAGER.Dialogs.createOptionsDialog();
				
					optionsDialog.header = "\(FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue) (\(accounts.count))";
					optionsDialog.options = options;
					optionsDialog.onSelect(handler: confirmListener);
					optionsDialog.multiSelect = true;
					optionsDialog.show();
			}, errorHandler: {
				(errorResponse) in
					loadingDialog.hide();
				
					guard let error: ApiErrorModel = errorResponse else {
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue,
																								   message: "Unable to parse the response.");
						alertDialog.show();
						return;
					}
				
					if (error.code == ErrorCode.REQUIRES_MFA.rawValue) {
						let mfaVerification: MFAVerificationModel = MFAVerificationModel(institutionId: self.credentialForm?.providerInstitutionId ?? "",
																						 label: error.messages[ErrorKey.MFA_PROMPT.rawValue],
																						 type: error.messages[ErrorKey.MFA_FIELD_TYPE.rawValue]);
						self.requestMFAVerification(credentials: self.credentialForm!,
													verification: mfaVerification);
					}
					else {
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue)\n(ERROR)",
							message: error.messages[ErrorKey.MESSAGE.rawValue] ?? "");
						alertDialog.show();
					}
			});
			
			do {
				loadingDialog.show();
				try msApi.financial().getLinkableAccounts(credentials: credentialForm!,
														  listener: linkAccountListener);
			}
			catch {
				print(error);
			}
		}
		else {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_LINKABLE_ACCOUNTS.rawValue)\n(ERROR)",
				message: "Please call \"Get Prompts\" first.");
			
			alertDialog.show();
		}
	}
	
	private func linkAccounts(accounts: [FinancialAccountLinkModel]?) {
		if (accounts != nil && accounts!.count > 0) {
			let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.LINK_ACCOUNTS.rawValue,
																							 message: "Attempting to link account(s), please wait...");
			let msApi: MoneysoftApi = MoneysoftApi();

			do {
				let linkListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
					(response) in
						loadingDialog.hide();
					
						guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
							let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.LINK_ACCOUNTS.rawValue,
																									   message: "Unable to parse the response.");
							alertDialog.show();
							return;
						}
					
						var linkedAccountsMsg: String = "";
					
						for account in accounts {
							linkedAccountsMsg.append("\(account.name) | \(account.balance)\n");
						}
					
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.LINK_ACCOUNTS.rawValue,
																							   message: linkedAccountsMsg);
					alertDialog.show();
				}, errorHandler: {
					(error) in
						loadingDialog.hide();

						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.LINK_ACCOUNTS.rawValue)\n(ERROR)",
							message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
						alertDialog.show();
				});
				
				loadingDialog.show();
				try msApi.financial().linkAccounts(accounts: accounts!,
                                                   includeTransactions: true,
												   listener: linkListener);
			}
			catch {
				print(error);
			}
		}
		else {
			let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.LINK_ACCOUNTS.rawValue)\n(ERROR)",
				message: "No accounts have been selected to link.");
			alertDialog.show();
		}
	}

	private func requestMFAVerification(credentials: InstitutionCredentialsFormModel,
										verification: MFAVerificationModel) {
		let tokenFld: UITextField = UITextField();
		
		tokenFld.borderStyle = UITextBorderStyle.line;
		tokenFld.autocapitalizationType = UITextAutocapitalizationType.none;
		tokenFld.isSecureTextEntry = true;
		
		let inputDialog: InputDialogController = MANAGER.Dialogs.createInputDialog();
		
		inputDialog.header = "Verification required...";
		inputDialog.addInput(label: "Token:",
							 component: tokenFld,
							 tag: 0);
		inputDialog.onSuccess {
			verification.savedValue = tokenFld.text;
			
			let msApi: MoneysoftApi = MoneysoftApi();
			let linkListener: ApiListener<ApiResponseModel> = ApiListener<ApiResponseModel>(successHandler: {
				(response) in
				// Do nothing here
			}, errorHandler: {
				(error) in
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.LINK_ACCOUNTS.rawValue)\n(ERROR)",
						message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
					alertDialog.show();
			});
			
			do {
				try msApi.financial().setMFA(verification: verification,
											 listener: linkListener);
			}
			catch {
				print(error);
			}
		}
		
		inputDialog.show();
	}
	
	private func prepareUpdateCredentials() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let accountsListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
			(accountsResponse) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = accountsResponse as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.UPDATE_CREDENTIALS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				var options: [String : String] = [ : ];

				for account in accounts {
					if (account.providerAccountId != nil) {
						options[account.name] = account.providerAccountId!;
					}
				}
			
				let optionsDialog: OptionsDialogController<String> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Select account";
				optionsDialog.options = options;
				optionsDialog.onSelect(handler: {
					(selection) in
						let selectedAccount: FinancialAccountModel? = accounts.first(where: {
							(account) -> Bool in
								return account.providerAccountId == selection?.first;
						});

						if (selectedAccount != nil) {
//							TODO
//							self.updateCredentials(account: selectedAccount!);
						}
				});
			
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: accountsListener);
		}
		catch {
			print(error);
		}
	}

	private func getAccount() {
		var options: [String : Int] = [ : ];
		let confirmListener: (([Int]?) -> Void) = {
			(selections) in
				guard let accountIds: [Int] = selections else {
					return;
				}

				let accountId: Int = accountIds.first ?? -1;
				self.getAccount(accountId: accountId);
		};

		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let accountsListener: ApiListListener = ApiListListener<FinancialAccountModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				for account in accounts {
					options[account.nickname ?? account.name] = account.financialAccountId;
				}

				let optionsDialog: OptionsDialogController<Int> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Accounts (\(options.count))";
				optionsDialog.options = options;
				optionsDialog.onSelect(handler: confirmListener);
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: accountsListener);
		}
		catch {
			print(error);
		}
	}

	private func getAccount(accountId: Int) {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNT.rawValue,
																						 message: "Retrieving account, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let getAccountListener: ApiListener<FinancialAccountModel> = ApiListener<FinancialAccountModel>(successHandler: {
			(accountResponse) in
				loadingDialog.hide();
			
				guard let account: FinancialAccountModel = accountResponse else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNT.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNT.rawValue,
																						   message: "\(account.name) | \(account.number) | Balance: \(account.balance)");
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNT.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccount(accountId: accountId,
											 listener: getAccountListener);
		}
		catch {
			print(error);
		}
	}

	private func getAccounts() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let getAccountsListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
			(accountsResponse) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = accountsResponse as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				var accountsMsg: String = "";

				for account in accounts {
					accountsMsg.append("\(account.name) | \(account.number) | Balance: \(account.balance)\n");
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						   message: accountsMsg);
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: getAccountsListener);
		}
		catch {
			print(error);
		}
	}

	private func updateTransactions() {
		var options: [String : FinancialAccountModel] = [ : ];
		let confirmListener: (([FinancialAccountModel]?) -> Void) = {
			(selections) in
				guard let accounts: [FinancialAccountModel] = selections else {
					return;
				}

				self.updateTransactions(financialAccounts: accounts);
		};

		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let accountsListener: ApiListListener = ApiListListener<FinancialAccountModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				for account in accounts {
					options[account.nickname ?? account.name] = account;
				}
			
				let optionsDialog: OptionsDialogController<FinancialAccountModel> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Accounts(\(options.count))";
				optionsDialog.options = options;
				optionsDialog.multiSelect = true;
				optionsDialog.onSelect(handler: confirmListener);
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: accountsListener);
		}
		catch {
			print(error);
		}
	}

	private func updateTransactions(financialAccounts: [FinancialAccountModel]) {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.UPDATE_TRANSACTIONS.rawValue,
																						 message: "Attempting to update transactions, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let transactionListener: ApiListListener<FinancialTransactionModel> = ApiListListener<FinancialTransactionModel>(successHandler: {
			(response) in
				loadingDialog.hide();
				guard let transactions: [FinancialTransactionViewModel] = response as? [FinancialTransactionViewModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.UPDATE_TRANSACTIONS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				var transactionsMsg: String = "";

				for transaction in transactions {
					transactionsMsg.append("\(transaction.name) | \(transaction.amount)\n");
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.UPDATE_TRANSACTIONS.rawValue,
																						   message: transactionsMsg);
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.UPDATE_TRANSACTIONS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			
			let updateOptions = UpdateTransactionOptions()
			updateOptions.startDate = Calendar.current.date(byAdding: Calendar.Component.month, value: -1, to: Date()) ?? Date();
			updateOptions.endDate = Date();

			try msApi.financial().updateTransactions(financialAccounts: financialAccounts,
													 options: updateOptions,
													 listener: transactionListener);
		}
		catch {
			print(error);
		}
	}

	private func unlinkAccount() {
		var options: [String : FinancialAccountModel] = [ : ];
		let confirmListener: (([FinancialAccountModel]?) -> Void) = {
			(selections) in
				guard let accounts: [FinancialAccountModel] = selections else {
					return;
				}

				self.unlinkAccounts(financialAccounts: accounts);
		};

		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let accountsListener: ApiListListener = ApiListListener<FinancialAccountModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				for account in accounts {
					options[account.nickname ?? account.name] = account;
				}
			
				let optionsDialog: OptionsDialogController<FinancialAccountModel> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Accounts (\(options.count))";
				optionsDialog.options = options;
				optionsDialog.multiSelect = true;
				optionsDialog.onSelect(handler: confirmListener);
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: accountsListener);
		}
		catch {
			print(error);
		}
	}


	private func unlinkAccounts(financialAccounts: [FinancialAccountModel]) {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.UNLINK_ACCOUNT.rawValue,
																						 message: "Attempting to unlink account(s), please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let unlinkAccountsListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
			(unlinkResponse) in
				loadingDialog.hide();
			
				guard let unlinkedAccounts: [FinancialAccountModel] = unlinkResponse as? [FinancialAccountModel] else {
					return;
				}

				var unlinkMsg: String = "";

				for account in unlinkedAccounts {
					unlinkMsg.append("\(account.name) | \(account.number)\n");
				}
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.UNLINK_ACCOUNT.rawValue,
																						   message: unlinkMsg);
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.UNLINK_ACCOUNT.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().unlinkAccounts(financialAccounts: financialAccounts,
												 listener: unlinkAccountsListener);
		}
		catch {
			print(error);
		}
	}

	private func prepareRefreshAccount() {
		var options: [String : FinancialAccountModel] = [ : ];
		let confirmListener: (([FinancialAccountModel]?) -> Void) = {
			(selections) in
				guard let accounts: [FinancialAccountModel] = selections else {
					return;
				}

				let account: FinancialAccountModel? = accounts.first;

				if (account != nil) {
					self.refreshAccount(account: account!);
				}
		};

		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let accountsListener: ApiListListener = ApiListListener<FinancialAccountModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				for account in accounts {
					options[account.nickname ?? account.name] = account;
				}

				let optionsDialog: OptionsDialogController<FinancialAccountModel> = MANAGER.Dialogs.createOptionsDialog();
			
				optionsDialog.header = "Accounts (\(options.count))";
				optionsDialog.options = options;
				optionsDialog.onSelect(handler: confirmListener);
				optionsDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: accountsListener);
		}
		catch {
			print(error);
		}
	}

	private func refreshAccount(account: FinancialAccountModel) {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.REFRESH_ACCOUNT.rawValue,
																						 message: "Attempting to refresh account, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let updateInstitutionListener: ApiListener<FinancialAccountModel> = ApiListener<FinancialAccountModel>(successHandler: {
			(response) in
				loadingDialog.hide();
			
				guard let account: FinancialAccountModel = response else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.REFRESH_ACCOUNT.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.REFRESH_ACCOUNT.rawValue,
																						   message: "\(account.name) | \(account.balance)");
				alertDialog.show();
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.REFRESH_ACCOUNT.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();

			let refreshOptions: RefreshAccountOptions = RefreshAccountOptions()
			refreshOptions.startDate = Calendar.current.date(byAdding: Calendar.Component.month, value: -1, to: Date()) ?? Date();
			refreshOptions.endDate = Date();
			refreshOptions.includeTransactions = true;

			try msApi.financial().refreshAccount(financialAccount: account,
												 options: refreshOptions,
												 listener: updateInstitutionListener);
		}
		catch {
			print(error);
		}
	}
	
	private func refreshAccounts() {
		let loadingDialog: LoadingDialogController = MANAGER.Dialogs.createLoadingDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																						 message: "Retrieving accounts, please wait...");
		let msApi: MoneysoftApi = MoneysoftApi();
		let getAccountsListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
			(accountsResponse) in
				loadingDialog.hide();
			
				guard let accounts: [FinancialAccountModel] = accountsResponse as? [FinancialAccountModel] else {
					let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.GET_ACCOUNTS.rawValue,
																							   message: "Unable to parse the response.");
					alertDialog.show();
					return;
				}

				let refreshListener: ApiListListener<FinancialAccountModel> = ApiListListener<FinancialAccountModel>(successHandler: {
					(response) in
						loadingDialog.hide();
					
						guard let accounts: [FinancialAccountModel] = response as? [FinancialAccountModel] else {
							return;
						}

						var message: String = "";

						for account in accounts {
							message.append("\(account.name) | \(account.balance)");
						}
					
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.REFRESH_ACCOUNTS.rawValue,
																								   message: message);
						alertDialog.show();
				}, errorHandler: {
					(error) in
						loadingDialog.hide();
					
						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.REFRESH_ACCOUNTS.rawValue)\n(ERROR)",
							message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
						alertDialog.show();
				});

				do {
					loadingDialog.show();
					
					let refreshOptions: RefreshAccountOptions = RefreshAccountOptions();

					refreshOptions.includeTransactions = true;
					try msApi.financial().refreshAccounts(financialAccounts: accounts,
														  options: refreshOptions,
														  listener: refreshListener);
				}
				catch {
					loadingDialog.hide();
					print(error);
				}
		}, errorHandler: {
			(error) in
				loadingDialog.hide();
			
				let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: "\(FinancialEndpoint.GET_ACCOUNTS.rawValue)\n(ERROR)",
					message: error?.messages[ErrorKey.MESSAGE.rawValue] ?? "");
				alertDialog.show();
		});

		do {
			loadingDialog.show();
			try msApi.financial().getAccounts(listener: getAccountsListener);
		}
		catch {
			print(error);
		}
	}
	
//	TODO
//	private func updateCredentials(account: FinancialAccountModel) {
//		let msApi: MoneysoftApi = MoneysoftApi();
//
//		if (credentialForm != nil) {
//			credentialForm?.providerInstitutionId = account.providerContainerId ?? "";
//
//			let usernamePrompt: InstitutionCredentialPromptModel? = credentialForm?.prompts.first(where: {
//				(prompt) -> Bool in
//				return (prompt.index == 1)
//			});
//
//			if (usernamePrompt != nil) {
//				usernamePrompt?.savedValue = "user01";
//			}
//
//			let passwordPrompt: InstitutionCredentialPromptModel? = credentialForm?.prompts.first(where: {
//				(prompt) -> Bool in
//				return (prompt.index == 2)
//			});
//
//			if (passwordPrompt != nil) {
//				passwordPrompt?.savedValue = "user01";
//			}
//
//			let credentialsListener: ApiListener<ApiResponseModel> = ApiListener<ApiResponseModel>(successHandler: {
//				(response) in
//					guard let updateResult: ApiResponseModel = response else {
//						let alertDialog: AlertDialogController = MANAGER.Dialogs.createAlertDialog(title: FinancialEndpoint.UPDATE_CREDENTIALS.rawValue,
//																								   message: "Unable to parse the response.");
//						alertDialog.show();
//						return;
//					}
//
////					MANAGER.Dialogs.presentMessage(title: "\(FinancialEndpoint.UPDATE_CREDENTIALS.rawValue)",
////						message: "Success: \(updateResult.success)\nThe credentials for: \(account.providerContainerId) are now saved!");
//			}, errorHandler: {
//				(error) in
////					MANAGER.Dialogs.presentError(title: "\(FinancialEndpoint.UPDATE_CREDENTIALS.rawValue)\n(ERROR)",
////												 error: error);
//			});
//
//			do {
////				MANAGER.Dialogs.presentLoading(title: "\(FinancialEndpoint.UPDATE_CREDENTIALS.rawValue)",
////											   message: "Updating credentials, please wait...");
//				try msApi.financial().updateCredentials(credentials: credentialForm!,
//														listener: credentialsListener);
//			}
//			catch {
//				print(error);
//			}
//		}
//		else {
////			MANAGER.Dialogs.presentMessage(title: "\(FinancialEndpoint.UPDATE_CREDENTIALS.rawValue)",
////										   message: "Unable to update credentials because no prompts were loaded.");
//		}
//
//	}
}
