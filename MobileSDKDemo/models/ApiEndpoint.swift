//
//  ApiEndpoint.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

class ApiEndpoint {
	let name: String;
	let action: (() -> Void);
	
	init(name: String,
		 action: @escaping (() -> Void)) {
		self.name = name;
		self.action = action;
	}
}
