//
//  Component.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 3/9/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class Component {
	let label: String;
	let component: UIView;
	let key: String;
	
	init(label: String,
		 component: UIView,
		 key: String) {
		self.label = label;
		self.component = component;
		self.key = key;
	}
}
