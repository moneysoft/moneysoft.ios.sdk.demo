//
//  ServiceFactory.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

class ServiceFactory: ServiceFactoryProtocol {
	func create(_ type: ServiceType) throws -> ServiceProtocol {
		switch (type) {
			case ServiceType.MAIN:
				return createMainService();
			case ServiceType.AUTHORISATION:
				return createAuthorisationService();
			case ServiceType.FINANCIAL:
				return createFinancialService();
			case ServiceType.TRANSACTIONS:
				return createTransactionService();
			default:
				break;
		}
		
		throw ServiceError.INVALID_SERVICE;
	}
	
	private func createMainService() -> MainService {
		return MainService();
	}
	
	private func createAuthorisationService() -> AuthorisationService {
		return AuthorisationService();
	}
	
	private func createFinancialService() -> FinancialService {
		return FinancialService();
	}
	
	private func createTransactionService() -> TransactionService {
		return TransactionService();
	}
}
