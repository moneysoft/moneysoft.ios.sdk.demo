//
//  ServiceFactoryProtocol.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

protocol ServiceFactoryProtocol {
	func create(_ type: ServiceType) throws -> ServiceProtocol;
}
