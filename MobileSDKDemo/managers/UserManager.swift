//
//  UserManager.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 17/12/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import MobileSDK

class UserManager {
	var financialAccounts: [FinancialAccountModel]? = nil;
	
	init() {
		financialAccounts = [];
	}
}
