//
//  Manager.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class Manager {
	internal let Dialogs: DialogManager;
	internal let Service: ServiceManager;
	internal var User: UserManager? = nil;
	
	init() {
		Dialogs = DialogManager();
		Service = ServiceManager();
		User = nil;
	}
	
	func configure() {
		let rootController: UIViewController? = UIApplication.shared.windows.last?.rootViewController;
		
		Dialogs.viewController = rootController;
		Service.viewController = rootController as? BaseController;
	}
}

let MANAGER: Manager = Manager();
