//
//  ServiceManager.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

class ServiceManager {
	var viewController: BaseController? = nil;
	var activeService: ServiceProtocol? {
		get {
			return _activeService;
		}
		set {
			_activeService = newValue;
			viewController?.displayEndpoints();
		}
	};
	
	private var _activeService: ServiceProtocol? = nil;
	
	init() {
		do {
			_activeService = try ServiceFactory().create(ServiceType.AUTHORISATION);
		}
		catch {
			print(error);
		}
	}
}
