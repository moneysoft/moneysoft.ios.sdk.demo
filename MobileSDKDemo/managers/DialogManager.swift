//
//  AlertManager.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit
import MobileSDK

class DialogManager {
	var viewController: UIViewController? = nil;
	
	func createInputDialog() -> InputDialogController {
		return InputDialogController(parentController: viewController!);
	}
	
	func createOptionsDialog<T: Equatable>() -> OptionsDialogController<T> {
		return OptionsDialogController<T>(parentController: viewController!);
	}
	
	func createLoadingDialog(title: String,
							 message: String) -> LoadingDialogController {
		return LoadingDialogController(title: title,
									   message: message,
									   parentController: viewController!);
	}
	
	func createAlertDialog(title: String,
						   message: String) -> AlertDialogController {
		return AlertDialogController(title: title,
									 message: message,
									 parentController: viewController!);
	}
}
