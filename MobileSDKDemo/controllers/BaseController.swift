
//
//  BaseController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 20/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class BaseController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	@IBOutlet weak var backBtn: UIButton!
	@IBOutlet weak internal var endpointsTblView: UITableView!
	internal var endpoints: [ApiEndpoint] = [];
	private let CELL_IDENTIFIER: String = "endpoint_cell";
	private let CELL_HEIGHT: CGFloat = 50.0;
	
	override func viewDidLoad() {
		super.viewDidLoad();
		
		endpointsTblView.register(UINib(nibName: "ApiEndpointCell", bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER);
		endpointsTblView.delegate = self;
		endpointsTblView.dataSource = self;
		displayEndpoints();
	}
	
	func displayEndpoints() {
		self.endpoints = MANAGER.Service.activeService?.getEndpoints() ?? [];
		
		DispatchQueue.main.async {
			self.endpointsTblView.reloadData();
		}
	}

	@IBAction func back(_ sender: UIButton) {
		MANAGER.Service.activeService?.back();
	}

	func tableView(_ tableView: UITableView,
				   heightForRowAt indexPath: IndexPath) -> CGFloat {
		return CELL_HEIGHT;
	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int {
		return endpoints.count;
	}
	
	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: ApiEndpointCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as! ApiEndpointCell;
		
		cell.updateDisplay(endpoint: endpoints[indexPath.row]);
		
		return cell;
	}
	
	func tableView(_ tableView: UITableView,
				   didSelectRowAt indexPath: IndexPath) {
		endpoints[indexPath.row].action();
		tableView.deselectRow(at: indexPath,
							  animated: false);
	}
	
//	internal var endpoints: [ApiEndpoint] = [];
	
//
//	override func viewDidLoad() {
//		super.viewDidLoad();
//
//		endpointsTblView.register(UINib(nibName: "ApiEndpointCell", bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER);
//		endpointsTblView.delegate = self;
//		endpointsTblView.dataSource = self;
//		endpointsTblView.reloadData();
//	}
//
//
//	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//		return 50;
//	}
//
//	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//		return endpoints.count;
//	}
//
//
}
