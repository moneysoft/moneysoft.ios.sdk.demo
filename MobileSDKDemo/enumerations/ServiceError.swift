//
//  ServiceError.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

enum ServiceError: Error {
	case INVALID_SERVICE
}
