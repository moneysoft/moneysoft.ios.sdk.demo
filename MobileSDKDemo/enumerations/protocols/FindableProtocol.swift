//
//  FindableProtocol.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation

protocol FindableProtocol {
	associatedtype S
	associatedtype T
	
	static var allValues: [T] {
		get
	}
	
	static func getByValue(value: S) -> T;
}
