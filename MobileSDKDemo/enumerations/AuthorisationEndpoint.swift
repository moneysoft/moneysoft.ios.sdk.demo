//
//  AuthenticationEndpoints.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation

enum AuthorisationEndpoint: String, FindableProtocol {
	typealias S = String
	typealias T = AuthorisationEndpoint
	
	case UNKNOWN = ""
	case ENVIRONMENT = "Choose Environment"
	case LOGIN = "Login"
	case LOGOUT = "Logout"
	
	static internal var allValues: [AuthorisationEndpoint] = [AuthorisationEndpoint.ENVIRONMENT,
															  AuthorisationEndpoint.LOGIN,
															  AuthorisationEndpoint.LOGOUT];
	
	static func getByValue(value: String) -> AuthorisationEndpoint {
		for enumValue in allValues {
			if (enumValue.rawValue.lowercased() == value.lowercased()) {
				return enumValue;
			}
		}
		
		return AuthorisationEndpoint.UNKNOWN;
	}
}
