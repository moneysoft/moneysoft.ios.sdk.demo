//
//  MainMenuEndpoint.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation

enum MainMenuEndpoint: String, FindableProtocol {
	typealias S = String
	typealias T = MainMenuEndpoint
	
	case UNKNOWN = ""
	case FINANCIAL = "Financial"
	case TRANSACTIONS = "Transactions"
	case USER = "User"
	
	internal static var allValues: [MainMenuEndpoint] = [MainMenuEndpoint.FINANCIAL,
														 MainMenuEndpoint.TRANSACTIONS,
														 /*MainMenuEndpoint.USER*/];
	
	static func getByValue(value: String) -> MainMenuEndpoint {
		for enumValue in allValues {
			if (enumValue.rawValue.lowercased() == value.lowercased()) {
				return enumValue;
			}
		}
		
		return MainMenuEndpoint.UNKNOWN;
	}
}
