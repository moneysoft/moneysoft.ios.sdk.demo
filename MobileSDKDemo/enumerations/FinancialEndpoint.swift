//
//  FinancialEndpoints.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation

enum FinancialEndpoint: String, FindableProtocol {
	typealias S = String
	typealias T = FinancialEndpoint
	
	case UNKNOWN = ""
	case FINANCIAL_INSTITUTIONS = "Get Institutions"
	case INSTITUTION_PROMPTS = "Get Prompts"
	case GET_LINKABLE_ACCOUNTS = "Get Linkable Accounts"
	case LINK_ACCOUNTS = "Link Accounts"
	case UPDATE_CREDENTIALS = "Update Credentials"
	case GET_ACCOUNT = "Get Account"
	case GET_ACCOUNTS = "Get Accounts"
	case GET_LINKABLE_ACCOUNTS_MFA = "Get Linkable Accounts (MFA)"
	case REFRESH_ACCOUNT = "Refresh Account"
	case REFRESH_ACCOUNTS = "Refresh Accounts"
	case UPDATE_TRANSACTIONS = "Update Transactions"
	case GET_TRANSACTIONS = "Get Transactions"
	case UNLINK_ACCOUNT = "Unlink Account(s)"
	
	internal static var allValues: [FinancialEndpoint] = [FinancialEndpoint.FINANCIAL_INSTITUTIONS,
														  FinancialEndpoint.INSTITUTION_PROMPTS,
														  FinancialEndpoint.GET_LINKABLE_ACCOUNTS,
														  FinancialEndpoint.GET_LINKABLE_ACCOUNTS_MFA,
														  FinancialEndpoint.LINK_ACCOUNTS,
//														  FinancialEndpoint.UPDATE_CREDENTIALS,
														  FinancialEndpoint.GET_ACCOUNT,
														  FinancialEndpoint.GET_ACCOUNTS,
														  FinancialEndpoint.REFRESH_ACCOUNT,
//														  FinancialEndpoint.REFRESH_ACCOUNTS,
														  FinancialEndpoint.UPDATE_TRANSACTIONS,
														  FinancialEndpoint.GET_TRANSACTIONS,
														  FinancialEndpoint.UNLINK_ACCOUNT];

	static func getByValue(value: String) -> FinancialEndpoint {
		for enumValue in allValues {
			if (enumValue.rawValue.lowercased() == value.lowercased()) {
				return enumValue;
			}
		}
		
		return FinancialEndpoint.UNKNOWN;
	}
}
