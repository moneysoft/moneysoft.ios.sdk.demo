//
//  TransactionEndpoint.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 14/8/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation

enum TransactionEndpoint: String, FindableProtocol {
	typealias S = String
	typealias T = TransactionEndpoint
	
	case UNKNOWN = ""
	case GET_TRANSACTIONS = "Get Transactions"
	case CATEGORIES = "Categories"
	
	internal static var allValues: [TransactionEndpoint] = [TransactionEndpoint.GET_TRANSACTIONS,
															TransactionEndpoint.CATEGORIES];
	
	static func getByValue(value: String) -> TransactionEndpoint {
		for enumValue in allValues {
			if (enumValue.rawValue.lowercased() == value.lowercased()) {
				return enumValue;
			}
		}
		
		return TransactionEndpoint.UNKNOWN;
	}
}
