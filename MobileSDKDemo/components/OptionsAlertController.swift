//
//  OptionsAlertController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 17/12/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class OptionsAlertController<T: Equatable>: UIViewController, UITableViewDelegate, UITableViewDataSource {
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var optionsTbl: UITableView!
	@IBOutlet weak var confirmBtn: UIButton!
	@IBOutlet weak var verticalBtnDivider: UIView!
	@IBOutlet weak var cancelBtn: UIButton!
	
	private let alertTitle: String;
	private let keys: [String];
	private let options: [String : T];
	private let multiSelect: Bool;
	private let selectionHandler: (([T]?) -> Void);
	private let CELL_IDENTIFIER: String = "optionCell";
	private var selections: [T]? = nil;
	
	init(title: String,
		 options: [String : T],
		 multiSelect: Bool = false,
		 onSelect: @escaping (([T]?) -> Void)) {
		self.alertTitle = title;
		self.keys = Array(options.keys).sorted(by: {
			(lhsKey, rhsKey) -> Bool in
				return (lhsKey.lowercased() < rhsKey.lowercased());
		});
		
		self.options = options;
		self.multiSelect = multiSelect;
		self.selectionHandler = onSelect;
		
		super.init(nibName: "OptionsAlertView",
				   bundle: nil);
		self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
		self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve;
		
		selections = [];
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad();
		
		titleLbl.text = alertTitle;
		
		optionsTbl.register(UINib(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER);
		optionsTbl.delegate = self;
		optionsTbl.dataSource = self;
		optionsTbl.reloadData();
		
		if (multiSelect) {
			confirmBtn.isHidden = false;
			verticalBtnDivider.isHidden = false;
		}
		else {
			confirmBtn.isHidden = true;
			confirmBtn.removeFromSuperview();
			verticalBtnDivider.isHidden = true;
		}
	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int {
		return keys.count;
	}
	
	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let key: String = keys[indexPath.row];
		let cell: OptionCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! OptionCell;
		
		cell.updateDisplay(label: key,
						   value: options[key] ?? "");
		cell.isMultiSelectable(multiSelect);
		
		if (multiSelect) {
			let index: Int = selections?.index(where: {
				(item) -> Bool in
					if (options[key] != nil) {
						return (item == options[key]!);
					}
				
					return false;
			}) ?? -1;
			
			if (index > -1) {
				cell.select(animate: false);
			}
			else {
				cell.deselect(animate: false);
			}
		}
		
		return cell;
	}
	
	func tableView(_ tableView: UITableView,
				   didSelectRowAt indexPath: IndexPath) {
		let key: String = keys[indexPath.row];
		
		if (multiSelect) {
			let index: Int = selections?.index(where: {
				(item) -> Bool in
					if (options[key] != nil) {
						return (item == options[key]!);
					}
				
					return false;
			}) ?? -1;

			let cell: OptionCell = tableView.cellForRow(at: indexPath) as! OptionCell;
			
			if (index > -1) {
				cell.deselect();
				selections?.remove(at: index);
			}
			else {
				if (options[key] != nil) {
					cell.select();
					selections?.append(options[key]!);
				}
			}
		}
		else {
			selections?.append(options[key]!);
			
			self.dismiss(animated: true,
						 completion: {
							self.selectionHandler(self.selections);
			});
		}
	}
	
	@IBAction func onConfirm(_ sender: UIButton) {
		self.dismiss(animated: true,
					 completion: {
		
			self.selectionHandler(self.selections);
		});
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
		self.dismiss(animated: true,
					 completion: nil);
	}
}
