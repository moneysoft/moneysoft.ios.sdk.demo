//
//  OptionCell.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 17/12/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class OptionCell: UITableViewCell {
	@IBOutlet weak var cellLbl: UILabel!
	@IBOutlet weak var selectableSwitch: UISwitch!
	
	private var optionValue: Any? = nil;
	
	func updateDisplay(label: String, value: Any) {
		cellLbl.text = label;
		self.optionValue = value;
		selectableSwitch.isUserInteractionEnabled = false;
	}
	
	func isMultiSelectable(_ selectable: Bool) {
		if (selectable) {
			selectableSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75);
			selectableSwitch.isHidden = false;
		}
		else {
			selectableSwitch.isHidden = true;
		}
	}
	
	func select(animate: Bool = true) {
		selectableSwitch.setOn(true,
							   animated: animate);
	}
	
	func deselect(animate: Bool = true) {
		selectableSwitch.setOn(false,
							   animated: animate);
	}
}
