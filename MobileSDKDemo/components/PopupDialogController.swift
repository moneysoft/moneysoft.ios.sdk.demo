//
//  PopupDialogController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 8/11/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class PopupDialogController : UIViewController {
	var header: String = "";
	internal var parentController: UIViewController? = nil;
	internal var success: (() -> Void)? = nil;
	internal var cancel: (() -> Void)? = nil;
	
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var contentScrollContainer: UIScrollView!
	@IBOutlet weak var buttonContainer: UIView!
	@IBOutlet weak var confirmBtn: UIButton!
	@IBOutlet weak var cancelBtn: UIButton!
	
	convenience init(parentController: UIViewController) {
		self.init(nibName: "PopupDialogView",
				  bundle: nil);
		self.parentController = parentController;
		self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
		self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve;
	}
	
	override func viewDidLoad() {
		titleLbl.text = header;
	}
	
	func show() {
		self.parentController?.present(self,
									   animated: false,
									   completion: nil);
	}
	
	func onSuccess(_ success: @escaping (() -> Void)) {
		self.success = success;
	}
	
	func onCancel(_ cancel: @escaping (() -> Void)) {
		self.cancel = cancel;
	}
	
	@IBAction func bindSuccess(_ sender: UIButton) {
		hide();
		success?();
	}
	
	@IBAction func bindCancel(_ sender: UIButton) {
		hide();
		cancel?();
	}
	
	internal func hide() {
		self.dismiss(animated: false,
					 completion: nil);
	}
}
