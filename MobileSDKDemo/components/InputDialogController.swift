//
//  InputDialogController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 8/11/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class InputDialogController: PopupDialogController, UITextFieldDelegate {
	internal var inputs: [UIView] = [];
	
	func addInput(label: String? = nil,
				  component: UIView,
				  tag: Int) {
		if (label != nil) {
			let labelFld: UILabel = UILabel();
			
			labelFld.text = label;
			inputs.append(labelFld);
		}
		
		if (component is UITextField) {
			(component as! UITextField).delegate = self;
		}
		
		inputs.append(component);
		component.tag = tag;
	}
	
	override func viewDidLoad() {
		super.viewDidLoad();
		
		var yPosition: Double = 0.0;
		
		for input in inputs {
			input.frame = CGRect(x: 5.0,
								 y: yPosition,
								 width: Double(contentScrollContainer.frame.size.width) - 10.0,
								 height: 30.0);
			contentScrollContainer.addSubview(input);
			
			yPosition += Double(input.frame.size.height) + 10.0;
		}
		
		contentScrollContainer.contentSize.height = CGFloat(yPosition);
	}
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		textField.resignFirstResponder();
		
		return false;
	}
}
