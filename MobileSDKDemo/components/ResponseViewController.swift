//
//  ResponseView.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 29/11/18.
//  Copyright © 2018 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class ResponseViewController: UIViewController {
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var backBtn: UIButton!
	@IBOutlet weak var responseTxtView: UITextView!
	
	private var endpointTitle: String? = nil;
	private var content: String? = nil;
	
	override func viewDidLoad() {
		titleLbl.text = endpointTitle;
		responseTxtView.text = content;
	}
	
	func setTitle(title: String = "") {
		endpointTitle = title;
	}
	
	func setResponse(response: String) {
		content = response;
	}
	
	@IBAction func back(_ sender: UIButton) {
		dismiss(animated: false, completion: nil);
	}
}
