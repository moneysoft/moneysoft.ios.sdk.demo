//
//  LoadingViewController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 23/1/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class LoadingDialogController: UIViewController {
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var messageLbl: UILabel!
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	var alertTitle: String;
	var message: String;
	internal var parentController: UIViewController? = nil;
	
	init(title: String,
		 message: String,
		 parentController: UIViewController) {
		self.alertTitle = title;
		self.message = message;
		self.parentController = parentController;
		
		super.init(nibName: "LoadingView",
				   bundle: nil);
		self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
		self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve;
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		titleLbl.text = alertTitle;
		messageLbl.text = message;
		loadingIndicator.transform = CGAffineTransform(scaleX: 2.0, y: 2.0);
	}
	
	func show() {
		self.parentController?.present(self,
									   animated: false,
									   completion: nil);
	}
	
	internal func hide() {
		self.dismiss(animated: false,
					 completion: nil);
	}
}
