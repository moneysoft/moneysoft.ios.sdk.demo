//
//  InputAlertViewController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 24/1/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class InputAlertViewController: UIViewController {
	@IBOutlet weak var titleLbl: UILabel!
	@IBOutlet weak var confirmBtn: UIButton!
	@IBOutlet weak var cancelBtn: UIButton!
	@IBOutlet weak var inputContainer: UIScrollView!
	
	private let alertTitle: String;
	private let inputs: [Component];
	private let confirmHandler: (() -> Void);
	
	public init(title: String,
				inputs: [Component],
				onSubmit: @escaping (() -> Void)) {
		self.alertTitle = title;
		self.inputs = inputs;
		self.confirmHandler = onSubmit;
		
		super.init(nibName: "InputAlertView",
				   bundle: nil);
		self.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
		self.modalTransitionStyle = UIModalTransitionStyle.crossDissolve;
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		let LABEL_HEIGHT: Double = 30.0;
		let FIELD_HEIGHT: Double = 30.0;
		
		titleLbl.text = alertTitle;
		
		if (inputContainer != nil) {
			var yOffset: Double = 0.0;
			
			for input in inputs {
				let label: UILabel = UILabel(frame: CGRect(x: 5.0,
														   y: yOffset,
														   width: Double(inputContainer.frame.size.width - 10.0),
														   height: LABEL_HEIGHT));

				label.text = input.label;
				inputContainer.addSubview(label);
				yOffset += LABEL_HEIGHT;
				
				let field: UIView = input.component;
				
				field.accessibilityIdentifier = input.key;
				field.frame = CGRect(x: Double(label.frame.origin.x),
									 y: yOffset,
									 width: Double(label.frame.size.width),
									 height: FIELD_HEIGHT);
				field.layer.borderWidth = 1.0;
				field.layer.borderColor = UIColor.lightGray.cgColor;
				field.layer.cornerRadius = 5.0;
				
				inputContainer.addSubview(field);
				yOffset += FIELD_HEIGHT;
			}
			
			inputContainer.contentSize.height = CGFloat(yOffset);
		}
	}
	
	@IBAction func onConfirm(_ sender: UIButton) {
		dismiss(animated: true,
				completion: {
					self.confirmHandler();
		});
	}
	
	@IBAction func onCancel(_ sender: UIButton) {
		dismiss(animated: true,
				completion: nil);
	}
}
