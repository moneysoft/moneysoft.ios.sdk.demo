//
//  OptionsDialogController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 8/11/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class OptionsDialogController<T : Equatable> : PopupDialogController, UITableViewDelegate, UITableViewDataSource {
	var multiSelect: Bool = false;
	internal var optionsTbl: UITableView? = nil;
	internal var options: [String : T] = [ : ];
	internal var keys: [String] = [];
	private let CELL_IDENTIFIER: String = "optionCell";
	private var selections: [T] = [];
	private var selectionHandler: (([T]?) -> Void)? = nil;
	
	override func viewDidLoad() {
		keys = Array(options.keys).sorted(by: {
			(lhsKey, rhsKey) -> Bool in
				return (lhsKey.lowercased() < rhsKey.lowercased());
		});
		
		super.viewDidLoad();
		
		if (optionsTbl == nil) {
			optionsTbl = UITableView(frame: CGRect(x: 0.0,
												   y: 0.0,
												   width: Double(contentScrollContainer.frame.size.width),
												   height: Double(contentScrollContainer.frame.size.height)));
		}
		
		optionsTbl?.register(UINib(nibName: "OptionCell", bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER);
		optionsTbl?.delegate = self;
		optionsTbl?.dataSource = self;
		optionsTbl?.reloadData();
		
		contentScrollContainer.addSubview(optionsTbl!);
	}
	
	func addOption(key: String,
				   value: T) {
		options[key] = value;
	}
	
	func onSelect(handler: @escaping (([T]?) -> Void)) {
		self.selectionHandler = handler;
	}
	
	func tableView(_ tableView: UITableView,
				   numberOfRowsInSection section: Int) -> Int {
		return options.count;
	}
	
	func tableView(_ tableView: UITableView,
				   heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 40.0;
	}
	
	func tableView(_ tableView: UITableView,
				   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let key: String = keys[indexPath.row];
		let cell: OptionCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER) as! OptionCell;
		
		cell.updateDisplay(label: key,
						   value: options[key] ?? "");
		cell.isMultiSelectable(multiSelect);
		
		if (multiSelect) {
			let index: Int = selections.index(where: {
				(item) -> Bool in
				if (options[key] != nil) {
					return (item == options[key]!);
				}
				
				return false;
			}) ?? -1;
			
			if (index > -1) {
				cell.select(animate: false);
			}
			else {
				cell.deselect(animate: false);
			}
		}
		
		return cell;
	}
	
	func tableView(_ tableView: UITableView,
				   didSelectRowAt indexPath: IndexPath) {
		let key: String = keys[indexPath.row];
		
		if (multiSelect) {
			let index: Int = selections.index(where: {
				(item) -> Bool in
				if (options[key] != nil) {
					return (item == options[key]!);
				}
				
				return false;
			}) ?? -1;
			
			let cell: OptionCell = tableView.cellForRow(at: indexPath) as! OptionCell;
			
			if (index > -1) {
				cell.deselect();
				selections.remove(at: index);
			}
			else {
				if (options[key] != nil) {
					cell.select();
					selections.append(options[key]!);
				}
			}
		}
		else {
			selections.append(options[key]!);
			
			selectionHandler?(selections);
			hide();
		}
	}
	
	override func bindSuccess(_ sender: UIButton) {
		super.bindSuccess(sender);
		
		if (multiSelect) {
			selectionHandler?(selections);
		}
	}
}
