//
//  AlertDialogController.swift
//  MobileSDKDemo
//
//  Created by Robbie Koorey on 12/11/19.
//  Copyright © 2019 Moneysoft. All rights reserved.
//

import Foundation
import UIKit

class AlertDialogController: UIAlertController {
	internal var parentController: UIViewController? = nil;
	
	convenience init(title: String,
					 message: String,
					 parentController: UIViewController) {
		self.init(title: title,
				   message: message,
				   preferredStyle: UIAlertControllerStyle.alert);
		
		addAction(UIAlertAction(title: "Ok",
								style: UIAlertActionStyle.default,
								handler: nil));
		
		self.parentController = parentController;
	}
	
	func show() {
		self.parentController?.present(self,
									   animated: false,
									   completion: nil);
	}
	
	internal func hide() {
		self.dismiss(animated: false,
					 completion: nil);
	}
}
